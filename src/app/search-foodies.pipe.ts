import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchFoodies'
})
export class SearchFoodiesPipe implements PipeTransform {

  transform(pipeData, pipeModifier): any {
    if (!pipeModifier) return pipeData;
    return pipeData.filter(eachItem => {
      return (
        eachItem['contestant'].toLowerCase().includes(pipeModifier.toLowerCase())
      )
    });
  }

}
