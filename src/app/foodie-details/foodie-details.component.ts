import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-foodie-details',
  templateUrl: './foodie-details.component.html',
  styleUrls: ['./foodie-details.component.css'],
  inputs: ['foodie']
})
export class FoodieDetailsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
