import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-foodie-items',
  templateUrl: './foodie-items.component.html',
  styleUrls: ['./foodie-items.component.css'],
  inputs: ['foodie']
})
export class FoodieItemsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
