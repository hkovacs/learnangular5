import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodieItemsComponent } from './foodie-items.component';

describe('FoodieItemsComponent', () => {
  let component: FoodieItemsComponent;
  let fixture: ComponentFixture<FoodieItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodieItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodieItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
