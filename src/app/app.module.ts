import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FoodieItemsComponent } from './foodie-items/foodie-items.component';
import { SearchFoodiesPipe } from './search-foodies.pipe';
import { FoodieDetailsComponent } from './foodie-details/foodie-details.component';

@NgModule({
  declarations: [
    AppComponent,
    FoodieItemsComponent,
    SearchFoodiesPipe,
    FoodieDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
